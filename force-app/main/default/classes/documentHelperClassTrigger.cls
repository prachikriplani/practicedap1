public class documentHelperClassTrigger {

    public static void methodForBeforeInsertionUpdation(List<Document__c> documentList){
        Date current = Date.today();
        for(Document__c document : documentList){
            
            if(document.Valid_From_Date__c < current && document.Valid_To_Date__c > current){
                
                document.isActive__c = true;
            }else{
                
                document.isActive__c = false;
            }
        }
    }
}
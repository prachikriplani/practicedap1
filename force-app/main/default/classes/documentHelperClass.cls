public class documentHelperClass {
   
    /*
    @AuraEnabled (cacheable=true)
    public static Map<Integer,List<Document__c>> getDocumentRecords(){
        
        List<Document__c> documentRecords = [ SELECT Name__c, Document_Type__c, Valid_From_Date__c, Valid_To_Date__c, isActive__c FROM Document__c ORDER BY Valid_From_Date__c ];

        Map<Integer,List<Document__c>> finalData = new Map<Integer,List<Document__c>>();
        
        for(Document__c document : documentRecords){
            
            Date fromDate = document.Valid_From_Date__c;
            Integer year = fromDate.year();
            
            if(finalData.containsKey(year)){
                
                finalData.get(year).add(document);
                
            }else{
                
                List<Document__c> documents = new List<Document__c>{document};
                finalData.put(year, documents);
            }
            
        }

        System.debug(finalData);

        return finalData;
    }*/

    @AuraEnabled (cacheable=true)
    public static  Map<Integer,List<Document__c>> getFilteredRecords(String type, String valid){

        Boolean validBoolean = Boolean.valueOf(valid);

        List<Document__c> documentRecords = new List<Document__c>();
        if(type != '' && valid != ''){

            documentRecords = [ SELECT Name__c, Document_Type__c, Valid_From_Date__c, Valid_To_Date__c, isActive__c FROM Document__c WHERE Document_Type__c =:type AND isActive__c=:validBoolean ORDER BY Valid_From_Date__c ];

        }else if(type == '' && valid != ''){

            documentRecords = [ SELECT Name__c, Document_Type__c, Valid_From_Date__c, Valid_To_Date__c, isActive__c FROM Document__c WHERE  isActive__c=:validBoolean ORDER BY Valid_From_Date__c ];

        }else if(type != '' && valid == ''){

            documentRecords = [ SELECT Name__c, Document_Type__c, Valid_From_Date__c, Valid_To_Date__c, isActive__c FROM Document__c WHERE  Document_Type__c =:type ORDER BY Valid_From_Date__c ];

        }else if(type == '' && valid == ''){

            documentRecords = [ SELECT Name__c, Document_Type__c, Valid_From_Date__c, Valid_To_Date__c, isActive__c FROM Document__c  ORDER BY Valid_From_Date__c ];

        }
        

        Map<Integer,List<Document__c>> finalData = new Map<Integer,List<Document__c>>();
        
        for(Document__c document : documentRecords){
            
            Date fromDate = document.Valid_From_Date__c;
            Integer year = fromDate.year();
            
            if(finalData.containsKey(year)){
                
                finalData.get(year).add(document);
                
            }else{
                
                List<Document__c> documents = new List<Document__c>{document};
                finalData.put(year, documents);
            }
            
        }

        System.debug(finalData);

        return finalData;
    }

   
}
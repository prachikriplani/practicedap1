public class accountOpportunityLWCHelper {
    
    @AuraEnabled
    public static List<Account> fetchingAccounts(){
        
        List<Account> accRecords =  [SELECT Id, Name FROM Account];

        return accRecords;
    }

    @AuraEnabled
    public static List<Contact> fetchingContacts(Id accountId){
        
        List<Contact> contactRecords = [SELECT Id, Name, FirstName, LastName, Email, Fax, Phone FROM Contact WHERE AccountId =:accountId];

        return contactRecords;
    }

    @AuraEnabled
    public static List<Pricebook2> fetchingPriceBooks(){
        
        List<Pricebook2> priceBookList = [SELECT Name, Id FROM Pricebook2 WHERE IsActive = true];

        return priceBookList;
    }

    @AuraEnabled
    public static List<PricebookEntry> fetchingEntries(Id priceBookId){
        
        List<PricebookEntry> entries = [SELECT Product2.Name, UnitPrice FROM PricebookEntry WHERE Pricebook2Id = :priceBookId];

        return entries;
    }

    @AuraEnabled
    public static void creatingRecords(Object data){
        
        List<Map<String,Object>> finalProducts = new List<Map<String,Object>>();
        accountOpportunityLWCWrapper finalData = (accountOpportunityLWCWrapper) JSON.deserialize(JSON.serialize(data), accountOpportunityLWCWrapper.class);
                
        System.debug(finalData.accountId);
        System.debug(finalData.closeDate);
        
        //Date closed = Date.valueOf(finalData.closeDate);
        
        Date closed = date.today();
        
        List<Object> products = (List<Object>) finalData.products;
        
        System.debug(products);
        
        for(Integer i = 0; i<products.size(); i++){
            
            System.debug(products[i]);
            
            Map<String,Object> detail = (Map<String,Object>) JSON.deserializeUntyped((String) JSON.serialize(products[i]));
            
            finalProducts.add(detail);
            
        }
        
        Opportunity opportunityRecord = new Opportunity(Name='Opp-1', AccountId=(Id)finalData.accountId, CloseDate=closed, StageName='Prospecting', Pricebook2Id = (Id)finalProducts[0].get('priceBook'));
        
        System.debug(opportunityRecord);
        Database.SaveResult result = Database.insert(opportunityRecord, False);
        
        Id opportunityId;
        
        if(result.isSuccess()){
            
            opportunityId = result.getId();
            System.debug(opportunityId);
            
        }else{
            
            List<Database.Error> errors = result.getErrors();
            for(Database.Error error: errors){
                
                System.debug(error.getMessage());
            }
            
        }
        
        List<OpportunityLineItem> oppProducts = new List<OpportunityLineItem>();
        
        for(Map<String,Object> product : finalProducts){
            
            OpportunityLineItem oppProd = new OpportunityLineItem(OpportunityId=opportunityId, Product2Id=(Id)product.get('product'), Quantity=Integer.valueOf(product.get('quantity')), UnitPrice=Integer.valueOf(product.get('unitPrice')), PricebookEntryId=(Id) product.get('entry'));
            
            System.debug(oppProd);
            
            oppProducts.add(oppProd);
        }
        
        System.debug(oppProducts);
        
        List<Database.SaveResult> results2 = Database.insert(oppProducts, False);
        
        for(Database.SaveResult res : results2){
            
            if(res.isSuccess()){
                
                System.debug(res.getId());
            }else{
                
                List<Database.Error> errors = res.getErrors();
                for(Database.Error error: errors){
                    
                    System.debug(error.getMessage());
                }
            }
        }
                                                                                      
    }
}
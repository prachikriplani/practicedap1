public with sharing class meetingWithContacts {
    
    @AuraEnabled (cacheable=true)
    public static List<Meeting__c> fetchingMeetings(){
        List<Meeting__c> meetingData = new List<Meeting__c>();
        try {
            meetingData = [SELECT Id, Priority__c, Subject__c, Name, Meeting_Link__c, Meeting_Date__c, Meeting_Time__c, Description__c FROM Meeting__c];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

        return meetingData;
    }
}
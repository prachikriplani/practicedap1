public class accountOpportunityLWCWrapper {

	public class Products {
		public String id;
		public Integer unitPrice;
		public String quantity;
		public String priceBook;
		public String entry;
		public String product;
	}

	public List<Products> products;
	public String accountId;
	public Date closeDate;

	/*
	public static accountOpportunityLWCWrapper parse(String json) {
		return (accountOpportunityLWCWrapper) System.JSON.deserialize(json, accountOpportunityLWCWrapper.class);
	}*/
}
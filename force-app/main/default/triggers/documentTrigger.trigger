trigger documentTrigger on Document__c (before insert, before Update) {

    List<Document__c> documentList = Trigger.New;
    
    Map<Id,Document__c> documentOldMap = Trigger.OldMap;
    
    if(Trigger.isBefore){
        
        if(Trigger.isInsert || Trigger.isUpdate){
            
            documentHelperClassTrigger.methodForBeforeInsertionUpdation(documentList);
        }
    }
}
import { LightningElement, wire, api, track } from 'lwc';
import fetchingAccounts from '@salesforce/apex/accountOpportunityLWCHelper.fetchingAccounts';
import fetchingContacts from '@salesforce/apex/accountOpportunityLWCHelper.fetchingContacts';
import fetchingPriceBooks from '@salesforce/apex/accountOpportunityLWCHelper.fetchingPriceBooks';
export default class AccountOpportunityBasic extends LightningElement {

    allAccounts = [];
    allContacts = [];
    allpriceBook = [];
    allProducts = [];

    showTable = false;
    // @wire(fetchingAccounts) myAccounts(response){

    //     if(response){
            
    //         console.log("response => ", response.data);
    //     }else if(error){

    //         console.log(error);
    //     }
    // }

    connectedCallback(){

        fetchingAccounts().then(result=>{

            this.allAccounts = result;

            console.log("accounts => ", this.allAccounts);
            
        }).catch(error=>{

            console.log("error => ", error);
        })

        fetchingPriceBooks().then(result=>{

            this.allpriceBook = result;

            console.log("priceBook => ", this.allpriceBook);
            
        }).catch(error=>{

            console.log("error => ", error);
        })

    }

    @api accountId = '';

    get optionsAccount() {

        let accountList = [];
        
        this.allAccounts.forEach((account)=>{

            let acc = {};
            acc.label = account.Name;
            acc.value = account.Id;
            accountList.push(acc);
        })
        return accountList;
    }

    contactId = '';

    get optionsContact() {

        let contactList = [];
        
        this.allContacts.forEach((contact)=>{

            let con = {};
            con.label = contact.Name;
            con.value = contact.Id;
            contactList.push(con);
        })
        return contactList;
    }

    get optionsPriceBook() {

        let priceBookList = [];
        
        this.allpriceBook.forEach((priceBook)=>{

            let price = {};
            price.label = priceBook.Name;
            price.value = priceBook.Id;
            priceBookList.push(price);
        })
        
        return priceBookList;
    }

    fetchContacts(event) {

        this.accountId = event.detail.value;
        
        console.log(this.accountId);

        fetchingContacts({accountId:this.accountId}).then(result=>{

            console.log(result);

            this.allContacts = result;

        }).catch(error=>{

            console.log(error);
        })

    }

    settingContactDetails(event){

        let contactId = event.detail.value;
        console.log(contactId);

        this.allContacts.forEach((contact)=>{

            if(contact.Id == contactId){

                this.template.querySelector('[data-id="conEmail"]').value = contact.Email;
                this.template.querySelector('[data-id="conFax"]').value = contact.Fax;
                this.template.querySelector('[data-id="conPhone"]').value = contact.Phone;
            }
        })
    }

    @api closeDate;

    @api priceBookId ='';
    fetchPriceBookEntry(event){

        this.priceBookId = event.detail.value;
        this.showTable = true;
    }

    createRecords(){

        this.closeDate = this.template.querySelector('[data-id="closeDate"]').value;
    }
}
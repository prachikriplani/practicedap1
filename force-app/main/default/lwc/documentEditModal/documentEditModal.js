import { api } from 'lwc';
import LightningModal from 'lightning/modal';
export default class DocumentEditModal extends LightningModal {

    objectName = 'Document__c';
    @api recordId;

    handleOkay() {

        this.close('okay');
    }

    updatingRecord(){
        console.log("updated =>", this.recordId);
        this.close('okay');

    }
}
import { api } from 'lwc';
import LightningModal from 'lightning/modal';
export default class DocumentNewModal extends LightningModal {

    objectApiName = 'Document__c';

    fields = ['Name__c', 'Document_Type__c', 'Valid_From_Date__c', 'Valid_To_Date__c'];

    handleOkay() {
        this.close('okay');
    }

    handleSuccess(event){
        console.log("recordId => ", event.detail.id);
        this.close('okay');
    }
}
import { LightningElement, wire, track, api } from 'lwc';
import MyModal from 'c/documentNewModal';
import MyModal2 from 'c/documentEditModal';
import getFilteredRecords from '@salesforce/apex/documentHelperClass.getFilteredRecords';
import {refreshApex} from '@salesforce/apex';
export default class DocumentBasic extends LightningElement {

    valueType = '';
    @api editId;
    get optionsType() {
        return [
            { label: 'PDF', value: 'PDF' },
            { label: 'CSV', value: 'CSV' },
            { label: 'DOC', value: 'DOC' },
            { label: 'XML', value: 'XML' },
        ];
    }

    valueValid = '';

    get optionsValid() {
        return [
            { label: 'True', value: "true" },
            { label: 'False', value: "false" }
        ];
    }
    
    validity = '';
    type = '';
    keys = [];
    records = {};
    @track recordList = [];
    currentOpen ;
    refreshData;
    @track sortBy;
    @track sortDirection;

    async addNewDocument() {
        const result = await MyModal.open();
        refreshApex(this.refreshData);
    }

    @wire(getFilteredRecords, {type:'$type', valid:'$validity'}) myRecords(result){
        this.refreshData = result;
        let data = result.data;

        if(data){

            this.records = JSON.parse(JSON.stringify(data));
            console.log("rceords", JSON.parse(JSON.stringify(this.records)));

            
            this.keys = Object.keys(this.records);
            console.log(this.keys);
            let tempList = [];
            this.keys.forEach((key)=>{

                let obj = {};
                obj['year'] = key;
                obj['list'] = this.records[key];
                
                tempList.push(obj);

            })
            this.recordList = JSON.parse(JSON.stringify(tempList));
            console.log("list of wire => ", this.recordList);

            this.documentData = this.records[this.currentOpen];
        }else if(result.error){

            console.log(result.error);
        }
    };

    @track documentData = [];

    @track columns = [
        {
            fieldName : 'Name__c',
            label : 'Title',
            type : 'text'
        },
        {
            fieldName : 'Valid_From_Date__c',
            label : 'Valid From',
            type: "date-local",
            typeAttributes: {
                day:'2-digit',
                month:'2-digit',
                year:'numeric',},
            timezone:"Asia/Kolkata",
            sortable : "true"
        },
        {
            fieldName : 'Valid_To_Date__c',
            label : 'Valid To',
            type: "date-local",
            typeAttributes: {
                day:'2-digit',
                month:'2-digit',
                year:'numeric',},
            timezone:"Asia/Kolkata",
            sortable : "true"
        },
        {
            fieldName : 'Document_Type__c',
            label : 'Document Type',
            type : 'text'
        },
        {
            fieldName : 'isActive__c',
            label : 'Is Valid',
            type : 'boolean'
        },
        {type: 'button-icon', typeAttributes: {  
            iconName: 'utility:edit',
            label: 'Edit',  
            name: 'edit',  
            variant: 'bare',
            alternativeText: 'edit',        
            disabled: false
        }}
    ]

    handleToggleSection(event) {
        let year = this.keys[0];
        year = event.detail.openSections;
        this.currentOpen = year;
        console.log(year);
        this.documentData = this.records[year];
    }

    filterByType(event){
        console.log("event", event);
        if(event){
            this.type = event.target.value;
        }
        console.log(this.type);
        refreshApex(this.refreshData);
        // getFilteredRecords({type:this.type, valid:this.validity}).then(result=>{

        //     this.records = JSON.parse(JSON.stringify(result));
        //     console.log("rceords", JSON.parse(JSON.stringify(this.records)));

            
        //     this.keys = Object.keys(this.records);
        //     console.log(this.keys);
        //     let tempList = [];
        //     this.keys.forEach((key)=>{

        //         let obj = {};
        //         obj['year'] = key;
        //         obj['list'] = this.records[key];
                
        //         tempList.push(obj);

        //     })

        //     this.recordList = JSON.parse(JSON.stringify(tempList));

        //     console.log("list of Imperitive => ",this.recordList);

            

        // }).catch(error=>{

        //     console.log(error);
        // })
    }

    filterByValidity(event){
        if(event){
            this.validity = event.target.value;
        }

        console.log(this.validity);
        refreshApex(this.refreshData);

        // getFilteredRecords({type:this.type, valid:this.validity}).then(result=>{

        //     this.records = JSON.parse(JSON.stringify(result));
        //     console.log("records", JSON.parse(JSON.stringify(this.records)));
            
        //     this.keys = Object.keys(this.records);
        //     console.log(this.keys);

        //     let tempList = [];
        //     this.keys.forEach((key)=>{

        //         let obj = {};
        //         obj['year'] = key;
        //         obj['list'] = this.records[key];
                
        //         tempList.push(obj);

        //     })
        //     this.recordList = JSON.parse(JSON.stringify(tempList));

        //     console.log("list of Imperitive => ",this.recordList);

            

        // }).catch(error=>{

        //     console.log(error);
        // })

    }

    handleSort(event){

        console.log(event.detail.fieldName);
        console.log(event.detail.sortDirection);
        this.sortBy = event.detail.fieldName;       
        this.sortDirection = event.detail.sortDirection;  
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
        
    }

    sortData(fieldName, sortDirection) {

        let sortResult = this.records[this.currentOpen]; 
        let parser = (v) => v;

        let column = this.columns.find(c=>c.fieldName===fieldName);

        if(column.type==='Date') {
          parser = (v) => (v && new Date(v));
        }

        let sortMult = sortDirection === 'asc'? 1: -1;

        this.records[this.currentOpen]= sortResult.sort((a,b) => {
          let a1 = parser(a[fieldName]), b1 = parser(b[fieldName]);
          let r1 = a1 < b1, r2 = a1 === b1;
          return r2? 0: r1? -sortMult: sortMult;
        });

        console.log(this.records[this.currentOpen]);
        this.documentData = this.records[this.currentOpen];
        
      }

    async handleRowActions(event){
        
        const row = event.detail.row;
        let rowId = JSON.parse(JSON.stringify(row));
        console.log(rowId.Id);
        this.editId = rowId.Id;
        const result = await MyModal2.open(
            {
                recordId : this.editId
            }
        );
        refreshApex(this.refreshData);
    }
    
}
import { LightningElement, api, track } from 'lwc';
import fetchingEntries from '@salesforce/apex/accountOpportunityLWCHelper.fetchingEntries';
import creatingRecords from '@salesforce/apex/accountOpportunityLWCHelper.creatingRecords';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class AccountOpportunityChild extends LightningElement {

    @api accountId;
    @api priceBookId;
    @api closeDate;

    showRow = false;
    allProducts = [];
    
    @track details = [];

    
    @track rows = [];
    connectedCallback(){

        console.log("account => ", this.accountId);
        console.log("pricebook => ", this.priceBookId);

        fetchingEntries({priceBookId:this.priceBookId}).then(result=>{

            this.allProducts = result;

            console.log(this.allProducts);
        
            
        }).catch(error=>{

            console.log(error);
        })
    }

    get optionsProduct() {

        let productList = [];

        this.allProducts.forEach((product)=>{

            let productObject = {};
            productObject.label = product.Product2.Name;
            productObject.value = product.Id + '-'+ product.Product2.Id;
            productList.push(productObject);
        })
        return productList;
    }

    filldetails(event){

        let quantity = event.target.value;

        let id = event.currentTarget.dataset.id;

        console.log(id);

        let entryId = '';

        let price = 0;

        this.details.forEach(element=>{

            if(element.id == id){

                element['quantity'] = quantity;
                entryId = element['entry'];
            }

        })

        this.allProducts.forEach(product=>{

            if(product.Id == entryId){

                price = product.UnitPrice;
            }
        })

        this.details.forEach(element=>{

            if(element.id == id){

                element['unitPrice'] = price;
            }

        })

        console.log(JSON.parse(JSON.stringify(this.details)));

        let collection = this.template.querySelectorAll(`[data-id="${id}"]`);

        for(let i=3; i<=4; i++){

            collection[i].value = price;
        }

        collection[5].value = parseFloat(price) * parseFloat(quantity);
        
    }

    addingProduct(event){

        let id = event.detail.value;
        let dataId = event.currentTarget.dataset.id;
        console.log(dataId);
        console.log("combo => ",id);
        let productId = id.split('-')[1];
        let entryId = id.split('-')[0];
        let obj = {
            "product" : productId,
            "entry" : entryId,
            "priceBook" : this.priceBookId,
            "closeDate" : this.closeDate,
            "quantity" : 0,
            "unitPrice" : 0,
            "id" : dataId
        }

        console.log(JSON.parse(JSON.stringify(obj)));
        this.details.push(obj);
    }

    addingRow(event){
        
        let length = this.rows.length;
        let tempRows = [];
        for(let i=0; i<=length; i++){
            let id = 'row' + (i+1);
            let obj = {
                'id' : id,
                'serial' : i+1
            }
            tempRows.push(obj);
        }
        this.rows = JSON.parse(JSON.stringify(tempRows));
        console.log(JSON.parse(JSON.stringify(this.rows)));
        this.showRow = true;
    }

    deletingAllRows(){
        this.showRow = false;
        this.details = [];
        this.rows = [];
    }

    deleteRow(event){

        let dataId = event.currentTarget.dataset.id;

        let remainingDetails = [];
        
        this.details.forEach(element=>{

            console.log("elementid ", element.id);
            console.log("dataid ", dataId);

            if(element.id != dataId){

                remainingDetails.push(element);
            }

        })

        this.details = JSON.parse(JSON.stringify(remainingDetails));

        let elementsToBeDeleted = this.template.querySelectorAll(`[data-id="${dataId}"]`);

        console.log(elementsToBeDeleted);

        // Array.prototype.forEach.call( elementsToBeDeleted, function( node ) {

        //     console.log(node);
        //     node.parentNode.removeChild( node );
            
        // });

        document.querySelectorAll(`[data-id="${dataId}"]`).forEach(e => e.remove());

        console.log("after deletion", JSON.parse(JSON.stringify(this.details)));

        let remainingRows = [];

        this.rows.forEach((row)=>{

            if(row.id != dataId){

                remainingRows.push(row);
            }

        })

        this.rows = JSON.parse(JSON.stringify(remainingRows));

        let count = 1;

        this.rows.forEach((row)=>{

            row.serial = count;
            count+=1;
        })

        console.log("after deletion", JSON.parse(JSON.stringify(this.rows)));
    }

    createRecords(){

        if(this.accountId == ''){

            let myToast = new ShowToastEvent(
                {
                    title : "Missing required fields",
                    variant : "error",
                    message : "Please fill in all required fields including account, pricebook and close date."
                }
            );
            this.dispatchEvent(myToast);

        }else {

            let finalData = {};
            finalData['accountId'] = this.accountId;
            finalData['closeDate'] = JSON.stringify(this.closeDate);
            finalData['products'] = this.details;

            console.log(JSON.parse(JSON.stringify(finalData)));
            creatingRecords({data:finalData}).then(result=>{

                console.log(result);

            }).catch(error=>{

                console.log(error);
            });
        }

    }
}
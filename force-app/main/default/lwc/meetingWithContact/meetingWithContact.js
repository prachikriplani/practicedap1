import { LightningElement, wire } from 'lwc';
import fetchingMeetings from '@salesforce/apex/meetingWithContacts.fetchingMeetings';
import { refreshApex } from '@salesforce/apex';
export default class MeetingWithContact extends LightningElement {

    refreshData;
    meetingData = [];
    @wire(fetchingMeetings) myMethod(result){
        if(result){
            this.refreshData = result;
            console.log(result);
            this.meetingData = result.data;
            console.log(this.meetingData);
        }
    }

    openTab(event) {
        console.log('value ==> ',event.target.value);
        let meeting = event.target.value;
        let i, tabcontent, tablinks;
        tabcontent = this.template.querySelectorAll(".tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
        }
        tablinks = this.template.querySelectorAll(".tablinks");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        console.log(`'[data-id="${meeting}"]'`);
        console.log('target ==> ',event.target);

        console.log(this.template.querySelector(`[data-id="${meeting}"]`));
        this.template.querySelector(`[data-id="${meeting}"]`).style.display = "block";
        event.target.class += " active";
      }
      
}